from django.contrib import admin

# Register your models here.
from restaurant.models import *

@admin.register(Restaurant)
class RestaurantAdmin(admin.ModelAdmin):
    list_display=['name','photo']
@admin.register(Customer)
class CustomerAdmin(admin.ModelAdmin):
    list_display = ['username', 'phone']