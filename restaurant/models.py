from django.contrib.auth.hashers import make_password
from django.contrib.auth.models import AbstractUser
from django.db import models

# Create your models here.
from dinnerapp.settings import STATIC_URL, MEDIA_URL


class Customer(AbstractUser):
    phone = models.IntegerField(null=True, blank=True)
    photo = models.ImageField(upload_to="users/%Y/%m/%d", null=True, blank=True)

    def get_image(self):
        if self.photo:
            return "{}{}".format(MEDIA_URL, self.photo)
        return "{}{}".format(STATIC_URL,
                             "https://cdn.pixabay.com/photo/2015/10/05/22/37/blank-profile-picture-973460_640.png")

    def validate_password(self, value: str):
        return (make_password(value))


class Restaurant(models.Model):
    name= models.CharField(max_length=255)
    photo = models.ImageField(upload_to='restaurants', null=True)