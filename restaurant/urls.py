from rest_framework import routers

from restaurant.apiviews import *

router = routers.DefaultRouter()
router.register('customer', CustomerViewSet)
router.register('restaurant', RestaurantViewSet)


